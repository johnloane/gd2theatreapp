package com.dkit.gd2.theatreapp;

import java.util.HashMap;
import java.util.Map;

public class MapDemo
{
    public static void main(String[] args) {
        Map<String, String> languages = new HashMap<>();
        languages.put("Java", "I don't know what to say about this one");
        languages.put("Javascript", "Loose language. Like Java, only worser");
        languages.put("C++", "Heeeeellllllllppppppppp, Yeeeeaaaaayyyyyyyy");
        languages.put("PHP", "The worst language ever, I'm glad its over");
        languages.put("Python", "I actually liked it");
        languages.put("Mysql", "Easy class");

        languages.remove("PHP");

        if(languages.remove("Python", "I actually liked it"))
        {
            System.out.println("Python removed");
        }
        else
        {
            System.out.println("Python not removed");
        }

        //Replace
        if(languages.replace("Mysql", "Easy class!", "My favourite class ever"))
        {
            System.out.println("Mysql replaced");
        }
        else
        {
            System.out.println("Mysql not replaced");
        }


        if(languages.containsKey("Java"))
        {
            System.out.println("Java is already in the map");
        }
        else
        {
            languages.put("Java", "Something about Java");
        }

        System.out.println(languages.put("Java", "My favourite language by far"));
        System.out.println(languages.put("Haskell", "A functional programming language"));

        printMap(languages);


    }

    public static void printMap(Map<String, String> mapToPrint)
    {
        for(String key : mapToPrint.keySet())
        {
            System.out.println(key + " : " + mapToPrint.get(key));
        }
    }
}
