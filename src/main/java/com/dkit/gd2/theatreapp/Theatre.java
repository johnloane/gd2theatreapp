package com.dkit.gd2.theatreapp;

import java.util.*;

public class Theatre {
    private final String threatreName;
    private final int numRows;
    private final int seatsPerRow;
    public List<Seat> seats = new ArrayList<Seat>();

    static final Comparator<Seat> PRICE_ORDER;

    static
    {
        PRICE_ORDER = new Comparator<Seat>() {
            @Override
            public int compare(Seat seat1, Seat seat2)
            {
                if(seat1.getPrice() < seat2.getPrice())
                {
                    return -1;
                }
                else if(seat1.getPrice() == seat2.getPrice())
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
        };
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public Theatre(String threatreName, int numRows, int seatsPerRow)
    {
        this.numRows = numRows;
        this.seatsPerRow = seatsPerRow;
        this.threatreName = threatreName;
        int lastRow = 'A' + (numRows-1);
        for(char row = 'A'; row <= lastRow; ++row)
        {
            for(int seatNum = 1; seatNum <= seatsPerRow; ++seatNum)
            {
                double price = 12.00;
                if((row < 'D') && (seatNum >=4 && seatNum <= 9) )
                {
                    price = 14.00;
                }
                else if((row > 'F') || (seatNum < 4 || seatNum > 9))
                {
                    price = 7.00;
                }
                Seat seat = new Seat(row + String.format("%02d", seatNum), price);
                seats.add(seat);
            }
        }
    }

    public String getThreatreName() {
        return threatreName;
    }

    //Write a method called reserveSeat that takes in a seatNumber and tries to reserve it
    //This may fail for at least two reasons - seat could already be reserved, or doesn't exist
    //The seat class has a method called reserve


    //This has O(n) complexity
    //Worst case is we search 96 seats
    public boolean reserveSeat(String seatNumber)
    {
        Seat requestedSeat = null;
        for(Seat seat : seats)
        {
            System.out.print(".");
            if(seat.getSeatNumber().equals(seatNumber))
            {
                requestedSeat = seat;
                break;
            }
        }

        if(requestedSeat == null)
        {
            System.out.println("There is no seat " + seatNumber);
            return false;
        }

        return requestedSeat.reserve();
    }

    //Binary search - worst case log_2(numberSeats)
    //For 96 the worst case will be 7 searches 7 < 96
    public boolean reserveSeatBS(String seatNumber)
    {
        int low = 0;
        int high = seats.size()-1;

        while(low <= high)
        {
            System.out.print(".");
            int mid = (low+high)/2;
            Seat midSeat = seats.get(mid);
            int cmp = midSeat.getSeatNumber().compareTo(seatNumber);

            if(cmp < 0)
            {
                low = mid + 1;
            }
            else if(cmp > 0)
            {
                high = mid - 1;
            }
            else
            {
                return seats.get(mid).reserve();
            }
        }
        System.out.println("There is no seat " + seatNumber);
        return false;
    }

    //print the seats, for testing
    public void printSeats()
    {
        for(int i=0; i < seats.size(); i++)
        {
            if(i % seatsPerRow == 0 && i != 0)
            {
                System.out.println();
            }
            System.out.print(seats.get(i).getSeatNumber() + " ");
        }
        System.out.println("\n================================================");

    }

    public class Seat implements Comparable<Seat>
    {
        private final String seatNumber;
        private double price;
        private boolean reserved = false;

        public Seat(String seatNumber, double price)
        {
            this.seatNumber = seatNumber;
            this.price = price;
        }

        public boolean reserve()
        {
            if(reserved == true)
            {
                System.out.println("Sorry, " + seatNumber + " is not available");
                return false;
            }
            else
            {
                this.reserved = true;
                System.out.println("Seat " + seatNumber + " reserved");
                return true;
            }
        }

        public boolean cancel()
        {
            if(this.reserved == false)
            {
                return false;
            }
            else
            {
                this.reserved = false;
                System.out.println("Reservation of seat " + seatNumber + " cancelled");
                return true;
            }
        }

        public String getSeatNumber() {
            return seatNumber;
        }

        public double getPrice() {
            return price;
        }

        @Override
        public int compareTo(Seat seat) {
            return this.seatNumber.compareToIgnoreCase(seat.getSeatNumber());
        }
    }


}
